def RHO(t, a_k, theta):
    M=2**t
    # Define a rho which gives back a matrix for a particular x,y () Basically gives back P(y)|y><y|
    
    def rho(x, y, t, a_k, theta):
        
        def Pr(t, y, a_k, theta):         # P(y,theta) for general case (multiple eigenstate) returns total sum +  
            List=[]                       # contribution from each terms
            import numpy as np
               
            for k in range(len(a_k)):
                numerator = 1 - np.cos((2*np.pi)*(y - theta[k]*M/(2*np.pi)))
                denominator = 1 - np.cos((2*np.pi/M)*(y - theta[k]*M/(2*np.pi)))
                term = (a_k[k]**2)*(numerator/denominator)
                List.append(term/M**2)
            
            return sum(List)
    
        def Multiply(A,B):              # A,B are array gives tensor product
            X=list(A[0]*B)              # multiply 0th term of A with all term of B and store it in X
            for i in range(1,len(A)):   
                X = X + list(A[i]*B)    # Loop concatenates/inserts next terms like 1st term of A multiplied by B in same X
            return X
    
        def columnmatrix(x,t):        # Creates |x> ket column term, here x is integer, we create computational basis 
            import numpy as np        # corresponding to that x depending on total size of t estimation register 
            x1=[]                     # example x=2, t=3 gives columnmatrix(x,t) =[0,0,0,0,0,0,1,0]
            Zero = np.array([1,0])    # qubit 0 state
            One = np.array([0,1])     # qubit 1 state
            if x==0:
                x1.append(x)
            while x != 0:
                x1.append(x%2)
                x = x//2 
            while len(x1)!= t:
                x1.append(0)
            
            x1.reverse()              # x1 is list containig binary form of x 
            x2 = np.array(x1)         # Now if we can do tensor product using matrices for 0 and 1 we get basis form of x
            if x2[0] == 0:            # If 0th term is 0 then begin tensor product using Multiply function   
                A = Zero
                for i in range(1,len(x2)):
                    if x2[i] == 0:
                        A = Multiply(A,Zero)
                    elif x2[i] == 1:
                        A = Multiply(A,One)
        
            elif x2[0] == 1:          # If 0th term is 1 then begin tensor product using Multiply function
                A = One
                for i in range(1,len(x2)):
                    if x2[i] == 0:
                        A = Multiply(A,Zero)
                    elif x2[i] == 1:
                        A = Multiply(A,One)
            return np.matrix(A).T     # make transpose of whole matrix (1D array) found using tensor product 
        
        def rowmatrix(y,t):           # Creates <y| bra row term, here y is integer, we create computational basis 
            import numpy as np        # same procepure as above but don't do transpose at last, this will keep row form
            x1=[]
            Zero = np.array([1,0])
            One = np.array([0,1])
            if y==0:
                x1.append(y)
            while y != 0:
                x1.append(y%2)
                y = y//2 
            while len(x1)!= t:
                x1.append(0)
            
            x1.reverse()
            x2 = np.array(x1)
            if x2[0] == 0:
                A = Zero
                for i in range(1,len(x2)):
                    if x2[i] == 0:
                        A = Multiply(A,Zero)
                    elif x2[i] == 1:
                        A = Multiply(A,One)
        
            elif x2[0] == 1:
                A = One
                for i in range(1,len(x2)):
                    if x2[i] == 0:
                        A = Multiply(A,Zero)
                    elif x2[i] == 1:
                        A = Multiply(A,One)
            return np.matrix(A)         # the whole matrix (1D array) found using tensor product 
        
        return columnmatrix(x,t)*rowmatrix(y,t)*round(Pr(t, y, a_k, theta),3) # this is P(y)|x><y| does work for only P(y)|y><y|
    
    Matrix = rho(0, 0, t, a_k, theta)   # this is P(0)|0><0|
    for j in range(1, M):
        Matrix += rho(j, j, t, a_k, theta)
        
    return Matrix                       # this is Sum(P(y)|y><y|)


