# Write a function that given a list of {a_k},[theta_k] a state |y>, estimation register size t → returns the probability Pr(y)
def Pr(t, y, a_k, theta):         # P(y,theta) for general case (multiple eigenstate) returns total sum +  
    list=[]                       # contribution from each terms
    import numpy as np
    M=2**t
    
    for k in range(len(a_k)):
        numerator = 1 - np.cos((2*np.pi)*(y - theta[k]*M/(2*np.pi)))
        denominator = 1 - np.cos((2*np.pi/M)*(y - theta[k]*M/(2*np.pi)))
        term = (a_k[k]**2)*(numerator/denominator)
        list.append(term/M**2)
            
    return sum(list), list



